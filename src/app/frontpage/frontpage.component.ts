import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.css'],
})
export class FrontpageComponent implements OnInit {
  isLoginMode = true;
  isLoading = false;
  error: string = null;

  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {}

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;

    if (!form.valid) {
      return;
    }

    this.isLoading = true;

    if (this.isLoginMode) {
      this.auth.login(email, password).subscribe(
        (resData) => {
          console.log(resData);
          this.isLoading = false;
          this.router.navigate(['/recipes']);
        },
        (errorRes) => {
          console.log(errorRes);
          switch (errorRes.error.error.message) {
            case 'EMAIL_NOT_FOUND':
              this.error = 'Nepostojeca email adresa. Pokusajte ponovo.';
              break;
            case 'INVALID_PASSWORD':
              this.error =
                'Sifra je neispravna. Proverite ispravnost i pokusajte ponovo.';
              break;
            default:
              this.error =
                'Greska! Nesto nije u redu. Pokusajte ponovo ili kontaktirajte tehnicku podrsku.';
              break;
          }
          this.isLoading = false;
        }
      );
    } else {
      this.auth.signup(email, password).subscribe(
        (resData) => {
          console.log(resData);
          this.isLoading = false;
          this.router.navigate(['/recipes']);
        },
        (errorRes) => {
          console.log(errorRes);
          switch (errorRes.error.error.message) {
            case 'EMAIL_EXISTS':
              this.error = 'Email adresa je zauzeta. Pokusajte ponovo.';
              break;
            default:
              this.error =
                'Greska! Nesto nije u redu. Pokusajte ponovo ili kontaktirajte tehnicku podrsku.';
              break;
          }
          this.isLoading = false;
        }
      );
    }

    form.reset();
  }
}
